const request = require('request')
const host = 'http://localhost:3000'

describe('Api', ()=>{
  it('Is listening', (done)=>{
        request(host)
        .get('/')
        .set('Accept', 'text/html')
        .expect('Content-Type', /text/)
        .expect(200, done)    
    })
})
